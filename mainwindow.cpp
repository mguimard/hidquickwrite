﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stringutils.h"
#include <sstream>
#include <thread>

using namespace std::chrono_literals;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    status = new QLabel();
    ui->statusbar->addPermanentWidget(status,10);

    ui->method->addItem("hid_write");
    ui->method->addItem("hid_send_feature_report");
    do_refresh();
}

void MainWindow::do_refresh()
{
    ui->devices->clear();
    devices.clear();

    hid_device_info* current_hid_device  = hid_enumerate(0,0);

    while(current_hid_device)
    {
        const char* manu_name = stringutils::wchar_to_char(current_hid_device->manufacturer_string);
        const char* prod_name = stringutils::wchar_to_char(current_hid_device->product_string);
        std::string device_string = stringutils::string_format("[%04X:%04X U=%04X P=0x%04X I=%d] %-20s - %-20s", current_hid_device->vendor_id, current_hid_device->product_id, current_hid_device->usage, current_hid_device->usage_page, current_hid_device->interface_number, manu_name, prod_name);

        ui->devices->addItem(QString::fromStdString(device_string));
        devices.push_back(current_hid_device);

        current_hid_device = current_hid_device->next;
    }

    hid_free_enumeration(current_hid_device);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_refresh_clicked()
{
    do_refresh();
}

void MainWindow::on_send_clicked()
{
    bool is_hid_write           = ui->method->currentIndex() == 0;
    unsigned int sleep_ms       = ui->sleep_ms->value();
    bool zero_pad               = ui->zeropad->isChecked();
    QString full_text           = ui->data->toPlainText();
    unsigned int device_index   = ui->devices->currentIndex();

    QStringList lines = full_text.split("\n");
    hid_device_info* info = devices[device_index];
    hid_device* dev = hid_open_path(info->path);

    ui->log->clear();

    if(dev)
    {
        ui->log->appendPlainText("Device opened, writing data...");

        for(QString line: lines)
        {
            line = line.trimmed();

            if(line.startsWith("//") || line.isEmpty())
            {
                continue;
            }

            QString packet = line.split("//")[0];

            if(zero_pad)
            {
                packet.prepend("00 ");
            }

            std::istringstream hex_chars_stream(packet.toStdString());
            std::vector<unsigned char> bytes_vector;
            unsigned int c;

            while (hex_chars_stream >> std::hex >> c)
            {
                bytes_vector.push_back(c);
            }

            unsigned int size = bytes_vector.size();
            uint8_t* data = &bytes_vector[0];

            int bytes = 0;

            ui->log->appendPlainText("Writing " + QString::number(size)+  " bytes...");

            if(is_hid_write)
            {
                bytes = hid_write(dev, data, size);
            }
            else
            {
                bytes = hid_send_feature_report(dev, data, size);
            }

            ui->log->appendPlainText("Bytes written: " + QString::number(bytes));

            if(bytes < 0)
            {
                std::string err = stringutils::string_format("Error: %ls", hid_error(dev));
                ui->log->appendPlainText(QString::fromStdString(err));
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
        }
        ui->log->appendPlainText("Done");
    }
    else
    {
        std::string err = stringutils::string_format("Cannot open device: %ls", hid_error(dev));
        status->setText(QString::fromStdString(err));
    }
}

void MainWindow::on_confirm_stateChanged(int value)
{
    ui->send->setEnabled(value);
}

void MainWindow::on_trim_clicked()
{

}
