#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <stdarg.h>
#include <memory>
#include <string>

class stringutils
{
public:
    static const char* wchar_to_char(const wchar_t* pwchar);
    static std::string string_format(const std::string fmt_str, ...);
};

#endif // STRINGUTILS_H
