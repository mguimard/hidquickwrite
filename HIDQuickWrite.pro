QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    stringutils.cpp

HEADERS += \
    mainwindow.h \
    dependencies/hidapi/hidapi/hidapi.h \
    stringutils.h

FORMS += \
    mainwindow.ui \

INCLUDEPATH += \
    dependencies/hidapi/ \

DEFINES += \
    USE_HID_USAGE \

win32:contains(QMAKE_TARGET.arch, x86_64) {
    LIBS += \
        -L"$$PWD/dependencies/hidapi-win/x64/" -lhidapi \
}

win32:contains(QMAKE_TARGET.arch, x86_64) {
    copydata.commands += $(COPY_FILE) \"$$shell_path($$PWD/dependencies/hidapi-win/x64/hidapi.dll            )\" \"$$shell_path($$DESTDIR)\" $$escape_expand(\n\t)
    first.depends = $(first) copydata
    export(first.depends)
    export(copydata.commands)
    QMAKE_EXTRA_TARGETS += first copydata
}

contains(QMAKE_PLATFORM, linux) {
    LIBS += \
        -lhidapi-hidraw \
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
