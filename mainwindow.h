#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "hidapi/hidapi.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_refresh_clicked();
    void on_send_clicked();
    void on_trim_clicked();
    void on_confirm_stateChanged(int);

private:
    Ui::MainWindow *ui;
    std::vector<hid_device_info*> devices;

    QLabel* status = nullptr;

    void do_refresh();
};
#endif // MAINWINDOW_H
